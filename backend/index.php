<?php
ini_set('display_errors', 'on');
date_default_timezone_set('Europe/Paris'); //...K windows
require 'vendor/autoload.php';
header("Access-Control-Allow-Origin: *");
require_once("ProductManager.php");
require_once("ClientManager.php");
require_once("src/Product.php");


use \Firebase\JWT\JWT;
use Tuupola\Base62Proxy as Base62;
use Slim\Http\Request;
use Slim\Http\Response;

//$app = new \Slim\App;


$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);



$jwt = new Tuupola\Middleware\JwtAuthentication([
    "path" => "/api",
    "secure" => false,
    "passthrough" => ["/client"],
    "secret" => "?/b{rzdA7VZ?@vL",
    "attribute" => "decoded_token_data",
    "algorithm" => ["HS256"],
    "error" => function ($response, $arguments) {

        $data = array('ERREUR' => 'ERREUR', 'ERREUR' => 'AUTO');
        return $response->withHeader("Content-Type", "application/json")->getBody()->write(json_encode($data));
    }

]);


    $app->get('/product/{id}', function (Request $request, Response $response, array $args) {

        global $entityManager;
        //$productRepo = $entityManager->getRepository('Product');
        $product = $entityManager->getRepository('Product')->findOneById((int)$args["id"]);
        if ($product != null) {
            $productArr["id"] = $product->getId();
            $productArr["nom"] = $product->getNom();
            $productArr["description"] = $product->getDescription();
            $productArr["prix"] = $product->getPrix();
            $productArr["stock"] = $product->getStock();
            $response->withStatus(200);
            return $response->withJson($productArr);
        } else {
            $response->withStatus(400);
            return $response->write((int)$args["id"]);
        }

    });

$app->get('/product', function (Request $request, Response $response, array $args) {

    global $entityManager;
    //$productRepo = $entityManager->getRepository('Product');
    $product = $entityManager->getRepository('Product')->findBy([],['id' => 'ASC']);
    if ($product != null) {
        $response->withStatus(200);
        return $response->withJson($product[0]);
    } else {
        $response->withStatus(400);
        return $response->write((int)$args["id"]);
    }

});

    $app->post('/product', function (Request $request, Response $response) {
        global $entityManager;
        $parsed = $request->getParsedBody();
        ProductManager::createProduct($parsed["description"], $parsed["nom"], $parsed["prix"], $parsed["stock"]);
        $response->withStatus(201);
        return $response->withJson("OK");
    });

    $app->put('/product/{id}', function (Request $request, Response $response, array $args) {

        global $entityManager;
        $parsed = $request->getParsedBody();
        $product = $entityManager->getRepository('Product')->findOneById((int)$args["id"]);
        if ($product != null) {
            if ($parsed["description"] != null) {
                $product->setDescription($parsed["description"]);
            }
            if ($parsed["prix"] != null) {
                $product->setPrix($parsed["prix"]);
            }
            if ($parsed["stock"] != null) {
                $product->setStock($parsed["stock"]);
            }

            if ($parsed["nom"] != null) {
                $product->setNom($parsed["nom"]);
            }

            $response->withStatus(200);
            return $response->withJson($product);
        } else {
            $response->withStatus(400);
            return $response->write((int)$args["id"]);
        }

    });

    $app->delete('/product/{id}', function (Request $request, Response $response, array $args) {

        global $entityManager;
        $product = $entityManager->getRepository('Product')->findOneById((int)$args["id"]);
        if ((int)$args["id"] != null) {
            $entityManager->remove($product);
            $entityManager->flush();
            return $response->withStatus(200);
        } else {
            $response->withStatus(400);
            return $response->write((int)$args["id"]);
        }
    });

    $app->get('/client/{login}', function (Request $request, Response $response, array $args) {

        global $entityManager;
        $client = $entityManager->getRepository('Client')->findOneByLogin($args["login"]);
        if ($client != null) {
            $clientArr["id"] = $client->getId();
            $clientArr["nom"] = $client->getNom();
            $clientArr["prenom"] = $client->getPrenom();
            $clientArr["tel"] = $client->getTel();
            $clientArr["addresse"] = $client->getAddresse();
            $clientArr["email"] = $client->getEmail();
            $clientArr["civilite"] = $client->getCivilite();
            $clientArr["password"] = $client->getPassword();
            $clientArr["login"] = $client->getLogin();
            $response->withStatus(200);
            return $response->withJson($clientArr);
        } else {
            $response->withStatus(400);
            return $response->write((int)$args["login"]);
        }

    });

    $app->post('/client', function (Request $request, Response $response) {
        $parsed = $request->getParsedBody();
        ClientManager::createClient($parsed["prenom"], $parsed["nom"], $parsed["civilite"], $parsed["addresse"], $parsed["tel"], $parsed["email"], $parsed["password"], $parsed["login"]);
        $response->withStatus(201);
        return $response->withJson("OK");
    });

    $app->put('/client/{id}', function (Request $request, Response $response, array $args) {

        global $entityManager;
        $parsed = $request->getParsedBody();
        $client = $entityManager->getRepository('Client')->findOneById((int)$args["id"]);
        if ($client != null) {
            if ($parsed["nom"] != null) {
                $client->setNom($parsed["nom"]);
            }
            if ($parsed["prenom"] != null) {
                $client->setPrenom($parsed["prenom"]);
            }
            if ($parsed["tel"] != null) {
                $client->setTel($parsed["tel"]);
            }
            if ($parsed["addresse"] != null) {
                $client->setAddresse($parsed["addresse"]);
            }
            if ($parsed["email"] != null) {
                $client->setEmail($parsed["email"]);
            }
            if ($parsed["civilite"] != null) {
                $client->setCivilite($parsed["civilite"]);
            }
            if ($parsed["password"] != null) {
                $client->setPassword($parsed["password"]);
            }
            if ($parsed["login"] != null) {
                $client->setLogin($parsed["login"]);
            }

            $response->withStatus(200);
            return $response->withJson($client);
        } else {
            $response->withStatus(400);
            return $response->write((int)$args["id"]);
        }

    });

    $app->delete('/client/{id}', function (Request $request, Response $response, array $args) {

        global $entityManager;
        $client = $entityManager->getRepository('Client')->findOneById((int)$args["id"]);
        if ((int)$args["id"] != null) {
            $entityManager->remove($client);
            $entityManager->flush();
            return $response->withStatus(200);
        } else {
            $response->withStatus(400);
            return $response->write((int)$args["id"]);
        }
    });

$app->post('/login', function ($request, $response, $args)
{
    $parsedBody = $request->getParsedBody();

    global $entityManager;

    $clientRepository = $entityManager->getRepository('Client');
    $client = $clientRepository->findOneBy(array('login' => $parsedBody["login"]));

    if($client != null)
    {
        $login = $client->getLogin();
        $password = $client->getPassword();

        if($parsedBody["login"] == $login) {
            $now = new DateTime("now");
            $future = new DateTime("now");
            $jti = Base62::encode(random_bytes(16));
            $secret = "?/b{rzdA7VZ?@vL";

            $payload = [
                    "login" => $client->getLogin(),
                    "id" => $client->getId()
            ];
            $token = JWT::encode($payload, $secret, "HS256");

            return $this->response->withJson(['token' => $token]);
        }
        else
        {
            return $response->withStatus(400);
        }
    }
    else{
        return $response->withStatus(401);
    }

});

$app->add($jwt);
$app->run();

?>