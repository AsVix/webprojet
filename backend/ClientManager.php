<?php
include("vendor/autoload.php");
include("bootstrap.php");

class ClientManager{
    public static function createClient($firstname,$lastname,$civilite,$address,$phone,$email,$password,$login)
    {
        //if($firstname.is_null() != null && $lastname != null && $civilite != null && $address != null && $phone != null && $email != null && $password != null && $login != null)
        //{
            global $entityManager;
            $client = new Client;
            $client->setNom($lastname);
            $client->setAddresse($address);
            $client->setPrenom($firstname);
            $client->setCivilite($civilite);
            $client->setTel($phone);
            $client->setEmail($email);
            $client->setPassword($password);
            $client->setLogin($login);
            $entityManager->persist($client);
            $entityManager->flush();
        //}
    }

    public static function findClientById($clientId)
    {
        global $entityManager;
        $clientRepo = $entityManager->getRepository('Client');
        return $clientRepo->findOneBy(array('id' => $clientId));
    }
}