<?php
include("vendor/autoload.php");
include("bootstrap.php");

class ProductManager{
    public static function createProduct($description,$nom,$prix,$stock)
    {
        if($stock >= 0 && $prix > 0)
        {
            global $entityManager;
            $product = new Product;
            $product->setNom($nom);
            $product->setDescription($description);
            $product->setPrix($prix);
            $product->setStock($stock);
            $entityManager->persist($product);
            $entityManager->flush();
        }
    }

    public static function findAllProduct()
    {
        global $entityManager;
        $ProductRepo = $entityManager->getRepository('Product');
        $productList = $ProductRepo->findAll();
        return $productList;
    }
}