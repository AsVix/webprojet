﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { ProductComponent } from './product/product.component';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_helpers';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'product', component: ProductComponent },


    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);