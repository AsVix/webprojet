import { Component, OnInit } from '@angular/core';
import {  Observable } from 'rxjs';

import { AuthenticationService } from '../_services/authentication.service';
import { ProductService } from '../_services/product.service';
import { Product } from '@/_models/product';
import { AlertService } from '@/_services/alert.service';

@Component({ templateUrl: 'product.component.html' })
export class ProductComponent {

    productList = new Array<any>();
    product: Product;
    cat: Observable<Product[]>;


    constructor(
        private productServive: ProductService,
        private alertService: AlertService,
        //private router: Router,

    ){}

    ngOnInit(){
        this.productServive.getProduct(4);
        this.cat = this.productServive.getAll()
    }


    onCliclDetail(description){ alert(description); }
}