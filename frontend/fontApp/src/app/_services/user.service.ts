﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    postId: any;
    constructor(private http: HttpClient) { }

    getAll() {
        this.http.get('http://localhost:8000/client/log').subscribe(data => {
            this.postId = data;
            console.log(this.postId);
        });
        return this.postId;
    }


    register(user: User) {
        console.log(JSON.stringify(user));
        this.http.post('http://localhost:8000/client', user).subscribe(data => {
            this.postId = data;
        });
        return this.postId;
    }

    delete(id: number) {
        return this.http.delete(`${config.apiUrl}/users/${id}`);
    }
}