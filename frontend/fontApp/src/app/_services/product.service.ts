import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Product } from '@/_models/product';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { request } from 'http';

@Injectable({ providedIn: 'root' })
export class ProductService {
    postId: any;
    data: any;
    constructor(private http: HttpClient) { }

    getAll() : Observable<Product[]> {
        return this.http.get<Product[]>('http://localhost:8000/product');
    }

    getProduct(id) /*: Observable<Product[]>*/ {
        this.http.get('http://localhost:8000/product/'+id).subscribe(data => {
            this.data = data;
        });
        console.log(this.data);
        return this.data;
        //return this.;
    }

    updateProduct(product: Product) {
        this.http.put('http://localhost:8000/product/'+ product.id,product).subscribe(data => {
            this.postId = data;
            console.log(this.postId);
        });
        return this.postId;
    }


    addProduct(product: Product) {
        console.log(JSON.stringify(product));
        this.http.post('http://localhost:8000/product', product).subscribe(data => {
            this.postId = data;
        });
        return this.postId;
    }

    deleteProduct(id: number) {
        return this.http.delete(`http://localhost:8000/product/`+id);
    }
}