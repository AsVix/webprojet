export class Product {
    id: number;
    nom: string;
    description: string;
    stock: number;
    prix: number;
    token: string;
}