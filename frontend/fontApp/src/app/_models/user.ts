﻿export class User {
    id: number;
    login: string;
    password: string;
    nom: string;
    prenom: string;
    tel: string;
    addresse: string;
    mail: string;
    civilite: string;
    token: string;
}